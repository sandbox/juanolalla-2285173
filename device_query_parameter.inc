<?php

/**
 * @file
 * Common functions for device_query_parameter module.
 */

 /**
  * Checks if the device query parameter value is the defined for mobile.
  */
function device_query_parameter_is_mobile() {
  $device = device_query_parameter_get_value();
  return $device == variable_get('device_query_parameter_mobile_value', 'mobile');
}

/**
 * Returns device parameter value from query string.
 */
function device_query_parameter_get_value() {
  $parameters = drupal_get_query_parameters();
  $key = variable_get('purl_method_device_query_parameter_provider_key');
  if (isset($parameters[$key])) {
    return $parameters[$key];
  }
  else {
    return FALSE;
  }
}

/**
 * Set a cookie to save the device used.
 */
function device_query_parameter_set_cookie_device($device = '') {
  return setcookie('device_query_parameter_device', $device, time() + 60 * 60 * 24 * 180, '/');
}

/**
 * Return the device cookie saved or false if there is not device cookie.
 */
function device_query_parameter_get_cookie_device() {
  if (isset($_COOKIE['device_query_parameter_device'])) {
    return $_COOKIE['device_query_parameter_device'];
  }
  else {
    return FALSE;
  }
}
