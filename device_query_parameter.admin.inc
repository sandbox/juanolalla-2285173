<?php

/**
 * @file
 * Admin settings form for device_query_parameter module.
 */

/**
 * Admin settings form.
 */
function device_query_parameter_settings_form() {
  global $theme_key;
  $default_theme = variable_get('theme_default', $theme_key);
  $form['device_query_parameter_mobile_theme'] = array(
    '#title' => 'Mobile theme',
    '#type' => 'select',
    '#options' => device_query_parameter_get_themes(),
    '#default_value' => variable_get('device_query_parameter_mobile_theme', $default_theme),
    '#required' => TRUE,
  );
  $form['device_query_parameter_mobile_value'] = array(
    '#title' => 'Parameter value for mobile',
    '#type' => 'textfield',
    '#size' => 25,
    '#default_value' => variable_get('device_query_parameter_mobile_value', 'mobile'),
    '#description' => t(
      'The key query parameter for the device provider must be set at !purl, with the "Query string" modifier type, previously enabled at !purl_types.',
      array(
        '!purl' => l(t('Persistent URL'), 'admin/config/search/purl'),
        '!purl_types' => l(t('Types'), 'admin/config/search/purl/types'),
      )
    ),
    '#required' => TRUE,
  );
  $form['device_query_parameter_desktop_value'] = array(
    '#title' => 'Parameter value for desktop',
    '#type' => 'textfield',
    '#size' => 25,
    '#default_value' => variable_get('device_query_parameter_desktop_value', 'desktop'),
    '#description' => t('This parameter is only used to choose the desktop view from a device detected as mobile, and is no persisted if cookies are allowed'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Helper function to determine the active themes.
 */
function device_query_parameter_get_themes() {
  $themes = array();

  foreach (list_themes() as $name => $value) {
    if ($value->status == 0) {
      continue;
    }
    $themes[$name] = preg_replace('/_/', ' ', ucfirst($value->name));
  }

  return $themes;
}
